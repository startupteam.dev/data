# Cookie Policy

StartupTeam is owned and operated by CosCoding, Inc (“StartupTeam“, “we”, “us”, and “our”). This Cookie Policy explains how we use cookies and similar technologies in the course of our business, including our websites that link to this Cookie Policy, such as startupteam.dev, or any website or mobile application owned, operated, or controlled by us (collectively, “Sites”). It explains what these technologies are, why and how we use them, as well as your rights with regards to controlling how they are used.

To turn off cookies, please see the "How do I turn cookies off?" section.

# What Are Cookies?

Cookies are small pieces of data stored by a website on your computer. Like many other sites, we use cookies to optimize your browsing experience. They allow us to maintain user preferences, provide reporting information, and personalize your content and experience. For example, if you have previously logged into your StartupTeam account on your device, a cookie is what allows us to log you in automatically the next time you visit!

# Why Do We Use Cookies?

We use cookies for several reasons. Some cookies are required for proper usage of our Websites and Services, which we refer to as “necessary” cookies. These cookies help us remember your preference for tools on our site.

Other cookies enable us and the third parties we work with to gather statistics by tracking the actions of visitors to our Site. A portion of these are used for advertising purposes, as they allow us to track and target visitor interests. In addition, third parties also serve cookies through our Websites and Services for advertising, analytics, and other purposes, as described in more detail below.

# How Does the Website Use Cookies?

A visit to a page on the website may generate the following types of cookies:

- Necessary cookies
    - __cfduid, Auth Token, Device ID, Sift Science, UID
- Analytics cookies
    - Amplitude, CMT, FullStory, Google Analytics, Hotjar, Optimizely, UTM
- Third-party cookies
    - AddThis, Algolia, Hubspot, and Stripe
- Third-party advertising cookies
    - Adsense, Facebook, Google Adwords, Perfect Audience, Quora, SteelHouse, and Twitter
- Necessary Cookies
    - When you register for an account with StartupTeam, we generate cookies that let us know whether you are signed in or not.

Our servers use these cookies to work out which account you are signed in with and recognizes you on returning visits. It also allows us to associate any requests, posts, and comments you post with your username.

While you are signed into our site, we combine information from these cookies with analytics cookies, which can be used to identify which pages you have seen on StartupTeam. For account security purposes and fraud prevention, if you register for a StartupTeam account, information such as your device fingerprint, browser fingerprint, and IP address will be collected.

Without these cookies, you may experience difficulties using our site.

## Analytics Cookies

Every time someone visits our website, an “anonymous analytics cookie” is generated.

These cookies tell us whether or not you have visited the site before. Your browser will tell us if you have these cookies, and, if you don’t, we generate new ones.

This allows us to track how many individual users we have, how often they visit the site, how our website is being used, and helps us customize our website for you and enhance your experience.

Without signing into StartupTeam, these cookies cannot be used to identify individuals, only devices. Logging in is what allows us to associate the data gathered by the “anonymous analytics cookie” to the specific user data tied to your StartupTeam account, such as your username and email address.

## Third-Party Cookies

StartupTeam uses various third-party tools and services to allow you to have a more personalized experience, and also enable features like social sharing. These third-party tools and services are primarily used for the following purposes:

## Social sharing

- Analytics
- Site Optimization and Customization
- Advertising
- Please see the following section for more information about Advertising cookies.

## Advertising Cookies

These cookies are used to make targeted advertising messages more relevant to you and your interests. They also prevent the same ad from continuously reappearing, ensure that ads are properly displayed, and may select ads based on your interests. These cookies are anonymous — they store information about what you are looking at on our site, but not about who you are.

## How Do I Turn Cookies Off?

When accessing our site, you have the right to accept or reject all cookies apart from those listed under the “Necessary Cookies” section, which are integral to proper site functionality. The full list has been copied below for your convenience:

- Necessary cookies
    - __cfduid, Auth Token, Device ID, Sift Science, UID
- Analytics cookies
    - Amplitude, CMT, FullStory, Google Analytics, Hotjar, Optimizely, UTM
- Third-party cookies
    - AddThis, Algolia, Hubspot, and Stripe
- Third-party Advertising cookies
    - Adsense, Facebook, Google Adwords, Perfect Audience, Quora, SteelHouse, and Twitter

If you are primarily concerned about third-party cookies generated by advertisers, you can turn these off by going to the Your Online Choices site or the Your Ad Choices site. You can also use this site to control all third-party online advertising.

All modern browsers allow you to change your cookie settings, though disabling them means you may be unable to use our site or will experience issues when using our site. You can usually find these settings in the Options or Preferences menu of your browser. To understand and adjust these settings, the following links may be helpful:

- [Cookie settings in Chrome](https://support.google.com/chrome/answer/95647?hl=en&ref_topic=14666)
- [Cookie settings in Firefox](http://support.mozilla.com/en-US/kb/Cookies)
- [Cookie settings in Safari web](https://support.apple.com/kb/PH17191?locale=en_US) and [iOS](http://support.apple.com/kb/HT1677)
- [Cookie settings in Internet Explorer](http://windows.microsoft.com/en-GB/internet-explorer/delete-manage-cookies#ie=ie-10)

## Changes to This Cookie Policy

We may update this Cookie Policy from time to time. We will notify you of any changes by posting the new Cookie Policy on our Site. You are advised to review this Cookie Policy periodically for any changes.

## Contact Us
If you have any questions about this Cookie Policy, please contact us at [support@startupteam.dev](mailto:support@startupteam.dev).
