# Terms of Service (“Agreement”)
This Agreement was last modified on July 29, 2020.

Please read these Terms of Service ("Agreement", "Terms of Service", “Terms”) carefully before using arc.dev ("Arc") and www.codementor.io ("Codementor"), collectively "Sites" or "Platforms", operated by Peeridea, Inc ("us", "we", or "our"). This Agreement sets forth the legally binding Terms and Conditions for your use of the Sites.

The Sites are platforms where Developers provide software development services to Clients. On Codementor, Clients can receive software development help via Live 1:1 Sessions, Freelance Jobs, Code Reviews, and Direct Payments. Our role is to facilitate the availability of the Sites and the services provided by Mentors. Codementor is not a provider of coding or mentoring services. Mentors are independent contractors and are not employees of Arc or Codementor.

By accessing or using the Sites in any manner, including, but not limited to, visiting or browsing the Sites or contributing content or other materials to the Sites, you agree to be bound by these Terms of Service. Capitalized terms are defined in this Agreement.

Additional definitions:

Clients (“Users”): users who request the help of experts on the Sites
Mentors (“Developers”): experts who provide services to Clients

## 1. Codementor Pre-Paid Credits and Featured Request Policies

Users may purchase Codementor Credits (“pre-paid credits”) to use on our Site. If a user needs to refund any unused pre-paid credits, the user will be charged a credit refund fee. Pre-paid credits are refundable up to 90 days from the time of purchase.

For every payment transaction on Codementor, there is a service and processing fee that is charged at the time of transaction.

Users may purchase the Featured Request option when creating a request for live 1:1 sessions, freelance jobs, code reviews, or long-term mentorships. Featured Requests places the user’s request at the top of the open request list provided to mentors for the first 12 hours. The Featured Request option is non-refundable.

## 2. Refund Policy & Dispute Resolution

Disputes are reviewed and handled on a case-by-case basis. If you have any questions about a Session, Freelance Job, Code Reviews, and/or Direct Payment, please email us before the payouts are confirmed to Mentors (on Sundays U.S. time).

After a dispute is initiated, if we do not receive a response from the Client or Mentor after 5 calendar days, we reserve the right to do the following:

For Live 1:1 Sessions: Close the dispute
For Suspended Freelance Jobs and Code Reviews: Release the disputed funds to the responding party, no matter if the job is suspended or completed
Please refer to the subsections below for more details about the specific policies for Live 1:1 Sessions, Freelance Jobs and Code Reviews (both Suspended and Completed), and Direct Payments.

When a dispute is being reviewed, Codementor reserves the right to use a third-party arbitration service to make a final decision about how the dispute should be handled.

### 2.1 Live 1:1 Sessions

Mentors can refund Clients at any time before the Mentor receives the payout for the session. Mentors are highly encouraged to refund Clients who are unsatisfied within reason during a live session. We reserve the right to pause or delay the payout for a session if a dispute arises, or when a dispute may potentially arise.

If a Client experiences an issue during a live 1:1 session, the Client must submit an invoice question at the end of the session. Codementor may reach out to the Client and/or Mentor to resolve the invoice question, and when this occurs, the Client and Mentor are required to reply to our emails within 5 calendar days, or stated period of time, unless the invoice question is resolved, otherwise we reserve the right to resolve the case in favor of the party that does respond to our emails (“responding party”).

An invoice question is considered resolved when it has been refunded by the Mentor or paid by the Client after the invoice question was created.

For sessions in which the Client did not submit an invoice question, Codementor Support must be contacted about the dispute within 5 calendar days after the end of the session to help the Client and Mentor initiate mediation. Codementor may reach out to the Client and/or Mentor to resolve the invoice question, and when this occurs, the Client and Mentor are required to reply to our emails within 5 calendar days, or stated period of time, unless the invoice question is resolved, otherwise we reserve the right to resolve the case in favor of the responding party.

### 2.1.1 Invoice question handling when Mentor or Client is unresponsive

If the Mentor does not respond to our emails about the invoice question within 5 calendar days, or stated period of time, the Mentor will not receive the payout for the session. If the Mentor has already received the payout for the session, the payout amount will be deducted from the Mentor’s future payouts.

If the Client does not respond to our emails about the invoice question within 5 calendar days, or stated period of time, the Client authorizes Codementor to charge the Client for the full cost of the session, and the Mentor will receive the payout for the session.

### 2.1.2 Invoice question handling when Mentor or Client is unresponsive

Once a session has ended, clients will be charged the invoice amount. In cases where the charge fails, Codementor reserves the right to suspend the client’s account until the invoice is paid. Regarding mentor payout for a session with an unpaid invoice, if fraud or disputes are not involved, Codementor reserves the right to determine the amount to be paid out with a maximum payout not exceeding more than 2 hours worth of session time.

## 2.2 Suspended Freelance Jobs and Code Reviews

After a Freelance Job or Code Review (“job”) is marked as complete, Mentors can refund Clients partially or in full at any time before the Client confirms the job is complete.

If a Mentor submits a Freelance Job or Code Review for review, but it was not completed according to the specifications agreed upon prior to the creation of the job, the Client must immediately suspend the job and discuss the concerns with the Mentor. Suspending the job will pause the payout to the Mentor so that a resolution can be reached. If the job is not suspended within 5 calendar days after the Mentor had submitted the job for review, the Client agrees that the payment on escrow should be released and sent to mentor and the payment is non-refundable.

A suspended job is considered resolved when it has been cancelled or marked as completed.

If the Client and/or Mentor needs assistance in coming to a resolution for the suspended job, Codementor Support must be contacted within 5 calendar days after the Mentor had submitted the job for review in order to initiate mediation.

Once the job is suspended by the Client, Codementor Support may also reach out to the Client and/or Mentor to initiate mediation. When this occurs, the Client and Mentor are required to reply to our emails within 5 calendar days, or stated period of time, unless the suspended job is resolved, otherwise we reserve the right to resolve the case in favor of the responding party.

During mediation, if we don’t receive a reply within 5 calendar days, or stated period of time, we reserve the right to resolve the case in favor of the responding party. If the responding party is the Client, the funds will be refunded in full to the Client. If the responding party is the Mentor, the funds will be released from suspension and processed to the Mentor.

If the Client and Mentor are unable to come to a resolution during the mediation, the Client and Mentor agree to allow Codementor to use a third-party arbitration service to make a final decision about how the dispute should be handled. The Client and Mentor agree to pay all arbitration fees and accept that the decision made by a third-party arbitrator is final and irrevocable.

Once an agreement has been reached by both parties, the Client should confirm the payment to release the payment from suspension to the Mentor. Freelance Jobs and Code Reviews that are marked/confirmed to be complete are non-refundable.

### 2.3 Completed Freelance Jobs and Code Reviews

Freelance Jobs and Code Reviews are considered “Completed” after one of the two following scenarios, after the Mentor has submitted his/her work for review (i.e. the “Pending Review” status): (i) after the Client clicks “Confirm Now” and confirms that the payment is non-refundable, and (ii) 5 calendar days after the Mentor’s work is Pending Review.

Clients are solely responsible for making sure that the work or service received was satisfactory before clicking “Confirm Now” or within 5 calendar days of the job’s Pending Review status, whichever comes first. All completed Freelance Jobs and Code Reviews are non-refundable.

### 2.4 Direct Payments

Direct Payments is to be used to pay for tips and to pay any additional charges, but not as a direct form of payment for help. Direct Payments is only to be used to compensate Mentors for already completed work or service, therefore the payment is non-refundable. Clients are responsible for making sure that the work or service received was satisfactory before creating the Direct Payment.

## 3. Identity and Payment Verification

User and payment verification on the Internet is difficult for online and card-not-present transactions, so we do not assume responsibility for the confirmation of any Client or Mentor’s identity. For transparency and fraud prevention purposes, when you register for a Codementor account and thereafter, your account, identity, and payment method may be subject to verification by our team. When requested, you must provide us with information about your identity and payment method in order to use our platform. During this time, Mentors may be asked to pause any ongoing work if the user has not provided Codementor with verification when requested.

You may be asked to provide any of the following: (i) one or more official forms of government identification, (ii) proof that your identity matches the provided identification, (iii) proof that you have authorized usage or ownership of your payment method, (iv) proof that the credit card associated with your account is physically present, as permitted by applicable laws.

You authorize Codementor to make any inquiries necessary to validate your identity and confirm your ownership of your payment method and/or financial accounts.

## 4. Intellectual Property

The Sites and its original content, features, and functionality are owned by Peeridea, Inc and are protected by international copyright, trademark, patent, trade secret and other intellectual property or proprietary rights laws.

Users and Mentors are responsible for the content they post. They assume all risks associated with this, including but not limited to a third-party’s reliance on its accuracy, claims related to intellectual property, and other legal rights. By posting content on Codementor, Users and Mentors are representing that they have the right to do so, and that doing so does not conflict with any other previous agreements made. Copyright infringement is not tolerated and may result in revoked publishing rights on Codementor and/or termination of your Codementor account.

### 4.1 Ownership of Work Product and Intellectual Property for Freelance Jobs, Mentorship Sessions, and Code Review

Upon Mentor’s receipt of full payment from Client, the Work Product of the Mentor will be the sole and exclusive property of the Client, and Client will be deemed to be the author thereof. The Mentor automatically irrevocably assigns to Client all right, title and interest worldwide in and to such Intellectual Property Rights and waives any moral rights, rights of paternity, integrity, disclosure and withdrawal or inalienable rights under applicable law in and to the Work Product.

### 4.2 Content Published on Codementor Community

Peeridea, Inc does not claim ownership of the content uploaded by its Users or Mentors to Codementor Community (https://www.codementor.io/community). For information on content licensing, see our community guidelines.

If you believe that content uploaded on our site infringes a copyright, please send a notice of copyright infringement to community@codementor.io.

You may NOT repurpose content on the Site without permission from the writer. You are encouraged to share links to our content instead. As the Site does not claim ownership of the content uploaded by users, if you’d like to use, modify, and build upon the code in posts found on your personal/commercial projects, feel free to contact the author of that content.

## 5. Code of Conduct
In using our Site and Services, you agree to follow our Code of Conduct. Violations of our Code of Conduct may result in the immediate removal of your access to this site and/or restrictions on your access to our services.

In general, maintaining civility in communication is encouraged. If you encounter someone who is negatively affecting your platform experience by either spamming you or insulting you, please report the user immediately.

We reserve the right to hide and/or remove any content on our site that may be illegal, unlawful, or unethical.

### 5.1 Mentor Code of Conduct

Mentors may not:

use their Codementor profile in a way that is considered outside of normal use, unethical, or illegal.
share their account with other developers. This means that the account creator should be the only user of that account at all times.
use aliases. Mentors should only use a real portrait photo for their profile picture.
solicit Clients to pay for services and/or have accepted payments from Clients through a Third-Party Platform, including sending refunds off-platform. We can only guarantee authorized payments made via our platform.
create multiple accounts to abuse the Site’s promotional or referral credits/program, or attempt fraud. Doing so will result in the confiscation of all unpaid payouts, and your status as a Mentor will be revoked.
engage in any form of cheating. This includes doing/completing graded assignments and/or tests for a Client or helping a Client cheat on assignments and/or tests. See also ‘6.2 Assignment Help’ below.
engage in plagiarism.
stalk, harass, or intimidate Clients about their rating or reviews. If you have any concerns about a particular user, please report the user.
make untruthful claims about your expertise or your ability to help.
make promises you do not intend to honor (including scheduled sessions, freelance jobs, and code reviews).
provide assistance to Clients who may attempt fraudulent activity, or whose accounts are flagged. Doing so may result in not receiving payouts for work provided to the Client(s).
Note: If your Client’s account is flagged while a Freelance Job or Code Review is in progress, you will need to pause the job until you receive permission from us to continue. Failing to do so may result in not receiving payouts for work completed.
If your account is found to violate any of the above guidelines, we reserve the right to revoke your status as a Mentor on our platform.

Being a mentor on Codementor does not imply that you are an employee of Codementor, but having a mentor profile on Codementor means that you may be perceived as an ambassador of our Site. If you exhibit any behavior outside of our site that may violate the Code of Conduct, we also reserve the right to revoke your status as a Mentor on our platform.

### 5.2 Client Code of Conduct

Clients may not:

use their Codementor profile in a way that is considered outside of normal use, unethical, or illegal.
share their account with other users. This means that the account creator should be the only user of that account at all times.
pay or attempt to or coerce Mentors into receiving payment via a Third-Party Platform. If you do so and the Mentor does not deliver, you will be fully responsible for your loss.
have more than one account in order to abuse the Site’s promotional or referral credits, attempt fraud, or for other unlawful or unethical reasons.
engage in any form of cheating. You should not request Mentors to do/complete assignments or tests for you, and we cannot assist you if a dispute arises. Should you need a mentor to guide you on how to do an assignment, this should be done during live paid mentorship sessions only. See also ‘6.2 Assignment Help’ below.
harass Mentors to provide you with free help.
use low ratings or negative reviews to coerce mentors into doing more for you, such as, but not limited to, additional work or lowering the charge.
be untruthful about an issue being unsolved just to receive a refund. We review every dispute on a case-by-case basis.
spam requests or send Mentors messages about things that are unrelated to the Services provided by this Site.
recruit Mentors to join third party services that are potentially competitive to Codementor. Please contact us if you'd like to discuss any partnerships.
If your account is found to violate any of the above guidelines, we reserve the right, but not limited to, to remove your account and/or restrict your access to our services.

## 6. Codementor Students Program and Assignments

### 6.1 Codementor Students Program

To be eligible for the Codementor Students Program, students must:

Be currently enrolled in a degree-granting course of study at an institution of higher education, such as college or university
Have a school-issued email address with a .edu domain
Be at least 18 years old
Once a student’s eligibility is verified, they will gain access to the benefits offered in the Students Program. Each student is only eligible for the program for the duration they’re enrolled in a course of study, up to a maximum of two years.

Students are required to follow their school's academic policies when getting help on Codementor. Engaging in any form of cheating may result in removal from the Students Program and the Codementor platform. This includes asking mentors to directly complete graded assignments and exams. Students are encouraged to get tutoring for coursework through mentorship sessions instead.

If a student is found in violation of these Terms, we reserve the right to terminate access to the Students Program and the Codementor platform.

### 6.2 Assignment Help

For help with assignments, whether part of the Codementor Students Program or not, Clients should be getting help during live paid mentorship sessions only where the mentor guides them and Clients do all the work. Clients may also be asked to provide proof that the request is not cheating, and we reserve the right to determine if cheating occurs.

Clients are not allowed to:

ask for assistance if it is considered cheating (this would be a violation of your school or organization).
get or pay for help using freelance jobs, code reviews, direct payments.
pay mentors off-platform.
ask or allow mentors to do any work for them.
Mentors are not allowed to:

do/complete any part of an assignment if it is considered cheating.
help with assessments, tests, exams, competitions, etc. as this is definitely cheating.
help and get paid using freelance jobs, code reviews, direct payments.
get paid off-platform.

## 7. Termination

We may terminate your access to the Site, without cause or notice, which may result in the forfeiture and destruction of all information associated with you. All provisions of this Agreement that by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity, and limitations of liability.

## 8. Limitation of Liability

By using our Services, you agree that Peeridea, Inc will not be liable for any indirect, incidental, special or consequential damages, loss of revenue, loss of data or emotional distress resulting from your use of the Site or services. We do not promise that the content/tutorials on our site are error-free.

## 9. Taxes

All Mentors are responsible for paying any applicable taxes associated with your use of the service in accordance to the governing laws of the country you reside in. All Mentors are required to fill in a tax form in order to receive payouts.

## 10. Links To Other Sites

Our Site may contain links to third-party sites that are not owned or controlled by Peeridea, Inc.

Trademarks of third-party sites, such as names and logos, may be used on our site to identify the sites. We do not claim any ownership of those names, slogans, or logos.

Peeridea, Inc has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party sites or services. We strongly advise you to read the terms and conditions and privacy policy of any third-party site that you visit.

## 12. Governing Law

This Agreement (and any further rules, policies, or guidelines incorporated by reference) shall be governed and construed in accordance with the laws of California, United States, without giving effect to any principles of conflicts of law.

## 12. Changes To This Agreement

We reserve the right, at our sole discretion, to modify or replace these Terms of Service by posting the updated terms on the Site. Your continued use of the Site after any such changes constitutes your acceptance of the new Terms of Service.

Please review this Agreement periodically for changes. If you do not agree to any of this Agreement or any changes to this Agreement, do not use, access or continue to access the Site or discontinue any use of the Site immediately.

## Contact Us

If you have any questions about this Agreement, please contact us at support@arc.dev.